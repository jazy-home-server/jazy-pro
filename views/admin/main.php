<?php

defined( 'ABSPATH' ) or die();
current_user_can( 'edit_themes' ) or wp_die( 'Insufficient permissions' );
current_user_can( 'edit_plugins' ) or wp_die( 'Insufficient permissions' );

$theme = wp_get_theme( J_THEME_NAME );

$sanitized_request = ! empty( $_REQUEST[ 'update' ] ) ? sanitize_text_field( $_REQUEST[ 'update' ] ) : '';
$output = '';
if ( 'plugin' === $sanitized_request ) {
    $updater = new \Jazy\Admin\PluginUpdater();
    $output = $updater->update();
} else if ( 'theme' === $sanitized_request ) {
    $updater = new \Jazy\Admin\ThemeUpdater();
    $output = $updater->update();
}

?>
<div class='wrap'>
    <div class='jazy-info'>
        <div class='jazy-info_output'>
        <?php if ( ! empty( $output ) ) : ?>
        <div><?php echo $output; ?></div>
        <?php endif; ?>
        </div>

        <div class='jazy-info_info'>
        The <?php echo J_THEME_NAME; ?> theme
        <?php if ( $theme->exists() ) : ?>
        exists.
        <?php else: ?>
        does not exist!
        <?php endif; ?>
        </div>
    </div>
    <div class='jazy-container'>
        <div class='jazy-row'>
            <form class='jazy-form' method='post'>
                <input type='hidden' name='update' value='plugin' />
                <input class='jazy-form_button' type='submit' name='update_submit' value='Update plugin' />
                <label class='jazy-form_label' for='update_submit'>Update the Jazy Pro plugin</label>
            </form>
        </div>
        <div class='jazy-row'>
            <form class='jazy-form' method='post'>
                <input type='hidden' name='update' value='theme' />
                <input class='jazy-form_button' type='submit' name='update_submit' value='Update theme' />
                <label class='jazy-form_label' for='update_submit'>Update the Jazy Pro child theme</label>
            </form>
        </div>
    </div>
</div>


