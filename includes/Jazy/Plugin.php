<?php

namespace Jazy;

defined( 'ABSPATH' ) or die();

class Plugin {
    function __construct() {
    }

    // Load dependencies
    function load() {
        jRequire( 'includes/Jazy/API/functions.php' );
        jRequire( 'includes/Jazy/Admin/functions.php' );
        wp_enqueue_style( 'jazy-admin', J_PLUGIN_URL . 'assets/css/admin.css' );
    }

    function init() {
        $this->load();

        register_activation_hook( __FILE__, [ $this, 'activate' ] );
        register_deactivation_hook( __FILE__, [ $this, 'deactivate' ] );

        add_action( 'admin_menu', '\Jazy\Admin\add_admin_menu' );
    }

    function activate() {
        $this->create_portfolio_post_type();
        flush_rewrite_rules();
    }

    function deactivate() {
        flush_rewrite_rules();
    }

    function create_portfolio_post_type() {
        register_post_type( 'portfolio', [
            'labels' => [
                'name' => 'Portfolios',
                'singular_name' => 'Portfolio',
            ],
            'description' => 'Personal project or work',
            'public' => true,
            'menu_icon' => 'dashicons-index-card',
            'supports' => [
                'title',
                'editor',
                'revisions',
                'comments',
                'excerpt',
                'thumbnail',
                'custom-fields',
            ],
            'show_in_rest' => true,
            'has_archive' => true,
        ] );
    }
}
