<?php
namespace Jazy\Admin;

defined( 'ABSPATH' ) or die();

class ThemeUpdater {
    private $THEME_REPO_URL = 'https://gitlab.com/jazy-home-server/jazy-pro-theme';
    private $theme;

    function __construct() {
        $this->theme = wp_get_theme( J_THEME_NAME );
    }

    function update() {
        if ( ! $this->theme->exists() ) {
            $themes_dir = get_theme_root();
            exec( 'cd ' . $themes_dir . ' && git clone ' . $this->THEME_REPO_URL );
        }
        $theme_dir = $this->theme->get_stylesheet_directory();
        $output1 = [];
        $output2 = [];
        exec( 'cd ' . $theme_dir . ' && git checkout master', $output1 );
        exec( 'cd ' . $theme_dir . ' && git pull origin master', $output2 );
        $output = implode( '</div><div>', $output1 );
        $output .= implode( '</div><div>', $output2 );
        return $output;
    }
}
