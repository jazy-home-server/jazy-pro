<?php
namespace Jazy\Admin;

defined( 'ABSPATH' ) or die();

class PluginUpdater {
    private $PLUGIN_REPO_URL = 'https://gitlab.com/jazy-home-server/jazy-pro';

    function update() {
        $output1 = [];
        $output2 = [];
        exec( 'cd ' . J_PLUGIN_PATH . ' && git checkout master', $output1 );
        exec( 'cd ' . J_PLUGIN_PATH . ' && git pull origin master', $output2 );
        $output = implode( '</div><div>', $output1 );
        $output .= implode( '</div><div>', $output2 );
        return $output;
    }
}
