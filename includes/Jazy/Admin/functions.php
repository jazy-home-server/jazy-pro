<?php
/*
 * General functions for admin related stuff.
 *
 * This file is in charge of exposing functions and requiring any specific classes
 */
namespace Jazy\Admin;

defined( 'ABSPATH' ) or die();

function add_admin_menu() {
    $perms = 'edit_themes';
    add_menu_page( 'JazyPro', 'Jazy Pro', $perms, 'jazy-main', '\Jazy\Admin\get_menu_page' );
    add_submenu_page( 'jazy-main', 'JazyPro Options', 'Options', $perms, 'jazy-main', '\Jazy\Admin\get_menu_page' );
    //add_submenu_page( 'jazy-main', 'Manage JazyPro Theme', 'Manage Theme', $perms, 'jazy-manage-jazypro-theme', '\Jazy\Admin\get_menu_page' );
}

function get_menu_page() {
    $current_page = isset( $_REQUEST[ 'page' ] ) ? sanitize_text_field( $_REQUEST[ 'page' ] ) : 'jazy-main';
    switch ( $current_page ) {
        case 'jazy-main': \Jazy\jRequire( 'views/admin/main.php' ); break;
        //case 'jazy-manage-jazypro-theme': \Jazy\jRequire( 'views/admin/theme-update.php' ); break;
    }
}
