<?php

/*
 * Remove plugin database data
 */

defined('WP_UNINSTALL_PLUGIN') or die();

// Remove portfolios from the DB
$portfolios = get_posts(['post_type' => 'portfolio', 'numberposts' => -1]);

foreach ($portfolios as  $portfolio) {
    wp_delete_post($portfolio->ID, true);
}

