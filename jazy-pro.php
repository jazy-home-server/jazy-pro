<?php
/**
 * @package Jazy-Pro
 * @version 1.0.0
 */
/*
Plugin Name: Jazy Professional
Plugin URI: http://jazyserver.com
Description: This plugin is made specifically for Jazy's professional Wordpress installation
Author: Jazy Llerena
Version: 1.0.0
Author URI: http://jazyllerena.com
*/

namespace Jazy;

defined( 'ABSPATH' ) or die();

// Constants
define( 'J_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'J_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'J_THEME_NAME', 'jazy-pro-theme' );

/*
 * Helper functions
 */

/*
 * Makes path relative to the plugin directory
 */
function jRequire( $path ) {
    require_once( J_PLUGIN_PATH . $path );
}

// Plugin loader
function jazy_load_plugin() {
    $jazyPro = new Plugin();
    $jazyPro->init();
}
add_action( 'init', '\Jazy\jazy_load_plugin' );

// Simple autoloader
function autoloader( $class_name ) {
    if ( false !== strpos( $class_name, 'Jazy' ) ) {
        $classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;
        $class_file = str_replace( '\\', DIRECTORY_SEPARATOR, $class_name ) . '.php';
        require_once( $classes_dir . $class_file );
    }
}
spl_autoload_register( '\Jazy\autoloader' );
